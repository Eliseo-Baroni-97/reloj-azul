function obtenerHora() {
    let fecha = new Date();
let pDiaSemana = document.getElementById("dia-semana");
let pDia = document.getElementById("dia");
let pMes = document.getElementById("mes");
let pAnio = document.getElementById("anio");
let pHora = document.getElementById("hora");
let pMinutos = document.getElementById("minutos");
let pSegundos = document.getElementById("segundos");
let pMeridiano = document.getElementById("meridiano");

let diasdeSemana =["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
let mesesdelAnio  =["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Nobiembre","Diciembre",]

pDiaSemana.innerText = diasdeSemana [fecha.getDay()];
pDia.innerText =fecha.getDay();
pMes.innerText = mesesdelAnio[fecha.getMonth()];
pAnio.innerText =fecha.getFullYear();
pHora.innerText =fecha.getHours();
pMinutos.innerText= fecha.getMinutes();
pSegundos.innerText=fecha.getSeconds();


if(fecha.getMinutes() < 10){
    pMinutos.innerText = "0" + fecha.getMinutes();
   }
if(fecha.getSeconds() < 10){
 pSegundos.innerText = "0" + fecha.getSeconds();
}

if (fecha.getHours() < 12) {
    pMeridiano.innerText = "AM";
    if (fecha.getHours() < 10) {
        pHora.innerText = "0" + fecha.getHours();
    }
} else {
    pMeridiano.innerText = "PM";
    if (fecha.getHours() == 12) {
        pHora.innerText = fecha.getHours();
    } else {
        if (fecha.getHours() < 22) {
            pHora.innerText = "0" + (fecha.getHours() - 12);
        } else {
            pHora.innerText = fecha.getHours() - 12;
        }
    }
}

}
window.setInterval(obtenerHora,1000);

